// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>

// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
InternetButton button = InternetButton();

String secondsElapsed = "";
String startButtonPressed = "no";
String secondsElapsedSpeed = "";
int delayTime = 0;
int j = 3;
int z = 9;

void setup() {
button.begin();

Particle.function("setSecondsElapsed", setSecondsElapsed);
Particle.function("setStartButtonPressed", setStartButtonPressed);
Particle.function("setSecondsElapsedSpeed", setSecondsElapsedSpeed);

Particle.variable("secondsElapsed", secondsElapsed);
Particle.variable("startButtonPressed", startButtonPressed);
Particle.variable("secondsElapsedSpeed", secondsElapsedSpeed);



}

void loop() {
if(button.buttonOn(1)){
button.allLedsOff();
startButtonPressed = "no";
}
if (startButtonPressed == "yes"){
int secondsInt = secondsElapsed.toInt();
int secondsSpeed = secondsElapsedSpeed.toInt();
if (secondsSpeed == 0){
delayTime = 5500;
}
else {
delayTime = secondsSpeed * 550;
}

for (int k = j;k<=z;k++){
button.ledOn(k,150,150,0);
button.ledOn(12-k,150,150,0);


}
delay(delayTime);
button.allLedsOff();
j = j + 1;
z = z - 1;


}
if (z <= 5){
button.allLedsOn(200,0,0);
}


}

int setSecondsElapsed(String cmd){
secondsElapsed = cmd;
return 1;
}
int setStartButtonPressed(String cmd){
startButtonPressed = cmd;
return 1;
}
int setSecondsElapsedSpeed(String cmd){
secondsElapsedSpeed = cmd;
return 1;
}


