//
//  ViewController.swift
//  Marking-itus-Test2Wearables
//
//  Created by AJAY BAJWA on 2019-11-07.
//  Copyright © 2019 ajay. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {
    
    //  User variables
    let USERNAME = "letv5050@gmail.com"
    let PASSWORD = "passion100"
    
    //  Device
    let DEVICE_ID = "38002a000247363333343435"
    var myPhoton : ParticleDevice?
    
    var kianiPresent = true
    var timeElapsed:Float = 0.0
    var timeElapsedSpeedKiani:Float = 0.0
    
    @IBOutlet weak var lblTimeElapsed: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // 1. Initialize the SDK
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
            }
        }// end login
        
        self.getDeviceFromCloud()
        self.lblTimeSlowDownBy.text = "Time Slows down by: 0.0 seconds"
    }
    
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon: \(device!.id)")
                self.myPhoton = device
            }
            
        }
    }

    @IBAction func btnStartMonitoring(_ sender: Any) {
        self.startTimer()
        self.sendStartButtonPressedToParticle()
        //self.timeElapsed = 0
    }
    
    public func startTimer(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//            if (self.kianiPresent == false){
//            self.timeElapsed = round((self.timeElapsed + 1)*10) / 10
//            }
            if (self.kianiPresent == true){
                self.timeElapsed = self.timeElapsed + 1 - self.timeElapsedSpeedKiani
                if (self.timeElapsed >= 20){
                    self.timeElapsed = 20
                }
            }
            self.sendTimeElapsedSpeedToParticle()
            self.lblTimeElapsed.text = String(format: "%.1f", self.timeElapsed)    //"\(self.timeElapsed)"
            
            self.startTimer()
            self.sendTimeElapsedToParticle()

        }
    }
 
    func sendTimeElapsedToParticle(){
        let funcArgs = ["\(self.timeElapsed)"] as [Any]
        let task = self.myPhoton!.callFunction("setSecondsElapsed", withArguments: funcArgs) { (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("sent time elapsed to particle")
            }
            else{
                print("Error sending time elapsed")
            }
        }
        
    }
    
    func sendTimeElapsedSpeedToParticle(){
        let funcArgs = ["\(self.timeElapsedSpeedKiani * 100)"] as [Any]
        let task = self.myPhoton!.callFunction("setSecondsElapsedSpeed", withArguments: funcArgs) { (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("sent time speed to particle")
            }
            else{
                print("Error sending time speed")
            }
        }
        
    }
    
    func sendStartButtonPressedToParticle(){
        let funcArgs = ["yes"] as [Any]
        let task = self.myPhoton!.callFunction("setStartButtonPressed", withArguments: funcArgs) { (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("sent start button status to particle")
            }
            else{
                print("Error sending start button status to particle")
            }
        }
        
    }
    
    @IBAction func btnKianiNotPresent(_ sender: Any) {
        self.kianiPresent = true
    }
    
    @IBAction func btnKianiPresent(_ sender: Any) {
        self.kianiPresent = true;
    }
    @IBOutlet weak var sliderOutlet: UISlider!
    
    
    @IBOutlet weak var lblTimeSlowDownBy: UILabel!
    @IBAction func sliderMohammadAmount(_ sender: Any) {
         let time = (sliderOutlet.value)/100 + 0.1
        self.timeElapsedSpeedKiani = round(time * 10) / 10
        self.lblTimeSlowDownBy.text = "Time Slows down by: \(self.timeElapsedSpeedKiani * 10) seconds"
        print("\(self.timeElapsedSpeedKiani*100)")
        
    }
}

